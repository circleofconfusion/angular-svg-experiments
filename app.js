angular
    .module("svgTest", [])
    .controller("AlertController", AlertController)


function AlertController() {
    var vm = this;

    vm.a = {alert:false};
    vm.b = {alert:false};
    vm.c = {alert:false};

    vm.toggleAlert = function(obj) {
       obj.alert = !obj.alert;
       console.table(vm);
    }
}
