# Angular SVG Experiments

This is meant to be a set of experiments around binding data from AngularJS
controllers to SVG elements.

The first experiment is a way to control whether a network node represented in a
SVG document can be dynamically set to an 'alert' or 'nominal' state. State can
be set either by clicking on elements in the image itself, or by clicking the
buttons above.